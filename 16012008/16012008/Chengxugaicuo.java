public class Chengxugaicuo {
    public static void main(String[] args) {
        Animal animal = new Dog();
        animal.shout();
        animal.run();
    }
}

class Animal {         //定义类
    void shout() {
        System.out.println("animal shout!");
    }
    void run(){
      System.out.println("animal run!");//父类中缺少能够跑的方法，子类就不能继承，所以得加上
    }
}

class Dog extends Animal {       //dog 继承animal的方法
    void shout() {
        super.shout();
        System.out.println("wangwang....");
    }

    void run() {
        System.out.println("Dog is running");
    }
}